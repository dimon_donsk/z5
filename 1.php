<?php
return [
    'about' => [
        'name' => 'Донской Дмитрий Сергеевич',
        'post' => 'Практикант',
        'email' => 'qwerty@mail.ru',
        'phone' => '88005553535',
        'site' => 'site.com'
    ],
    'education' => [
        [
            'faculty' => 'Программирование в компьютерных системах',
            'univercity' => 'СКС',
            'yearStart' => 2016,
            'yearEnd' => 2020
        ],
        [
            'faculty' => 'Программирование в компьютерных системах 1',
            'univercity' => 'СКС 1',
            'yearStart' => 2012,
            'yearEnd' => 2016
        ],
    ],
    'languages' => [
        'Russian', 'English'
    ],
    'interests' => [
        'увлечение 1', 'увлечение 2'
    ],
    'aboutCareer' => 'qwerty',

    'career' => [
        [
            'post' => 'post 1',
            'place' => 'City 1',
            'duty' => 'qwerty',
            'yearStart' => 2012,
            'yearEnd' => 2016
        ],
        [
            'post' => 'post 2',
            'place' => 'City 2',
            'duty' => 'qwerty123',
            'yearStart' => 2012,
            'yearEnd' => 2016
        ],
    ]
];